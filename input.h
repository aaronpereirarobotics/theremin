

#include "dhdc.h"
#include "drdc.h"

#ifndef INPUT_H
#define INPUT_H


class InputCalculator
{
  private:
  int brake_counter;
  int t1;
  int t2;
  double epsilon;

  public:
  int * mode = 0;
  double * p0; // start of array
  double * throttle;
  double * steer;
  double * pan;
  double * tilt;
  double * turn;


  InputCalculator();
  void setValues(int * mode_, double *p0_, double *throttle_, double *steer_, double *pan_, double *tilt_, double *turn_);
  //void setValues(int mode_, double p_[DHD_MAX_DOF]&, double throttle_&, double steer_&, double pan_&, double tilt_&);
  void calculate_input();
  void calculate_input_toy_car();
  void calculate_input_steering_wheel();
  void calculate_input_joystick_mode();
  void calculate_input_spot_turn();

};

#endif
