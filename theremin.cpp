//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D.
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE. 

    \author    <http://www.chai3d.org>
    \author    Federico Barbagli
    \author    Chris Sewell
    \author    Francois Conti
    \version   3.2.0 $Rev: 1925 $
*/
//==============================================================================

#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include "Eigen/Eigen"
using namespace Eigen;


#define M_PI           3.14159265358979323846
#define MAX_Z          0.12
#define MIN_Z          -0.12

//------------------------------------------------------------------------------
#include "chai3d.h"
#include "feedback.h"
#include "dhdc.h"
#include "drdc.h"
//------------------------------------------------------------------------------
#include <GLFW/glfw3.h>
//------------------------------------------------------------------------------
using namespace chai3d;
using namespace std;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// GENERAL SETTINGS
//------------------------------------------------------------------------------

// stereo Mode
/*
    C_STEREO_DISABLED:            Stereo is disabled 
    C_STEREO_ACTIVE:              Active stereo for OpenGL NVDIA QUADRO cards
    C_STEREO_PASSIVE_LEFT_RIGHT:  Passive stereo where L/R images are rendered next to each other
    C_STEREO_PASSIVE_TOP_BOTTOM:  Passive stereo where L/R images are rendered above each other
*/
cStereoMode stereoMode = C_STEREO_DISABLED;

// fullscreen mode
bool fullscreen = false;

// mirrored display
bool mirroredDisplay = false;


//------------------------------------------------------------------------------
// DECLARED VARIABLES
//------------------------------------------------------------------------------


cWorld* world;				// a world that contains all objects of the virtual environment
cCamera* camera;			// a camera to render the world in a window display
cSpotLight *light;			// a light source to illuminate the objects in the virtual scene
cHapticDeviceHandler* handler;		// a haptic device handler
cGenericHapticDevicePtr hapticDevice;	// a pointer to the current haptic device
cToolCursor* tool;			// a virtual tool representing the haptic device in the scene
cBackground* background;		// a colored background
cFontPtr font;				// a font for rendering text
cLabel* labelRates;			// a label to display the rate [Hz] at which the simulation is running
cScope* scope;				// a scope to monitor the sound signal
cMultiMesh* turntable;			// a virtual turntable
cMesh* record;				// a virtual vinyl
cAudioDevice* audioDevice;		// audio device to play sound
cAudioBuffer* audioBuffer1;		// audio buffer to store a sound file
cAudioSource* audioSource1;		// audio source which plays the audio buffer
cAudioSource* audioSource[16];		// audio source which plays the audio buffer
cAudioBuffer* audioBuffer2;		// audio buffer to store a sound file
cAudioSource* audioSource2;		// audio source which plays the audio buffer
cAudioBuffer* audioBuffer3;		// audio buffer to store a sound file
cAudioSource* audioSource3;		// audio source which plays the audio buffer
cAudioBuffer* audioBuffer4;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer5;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer6;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer7;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer8;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer9;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer10;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer11;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer12;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer13;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer14;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer15;		// audio buffer to store a sound file
cAudioBuffer* audioBuffer16;		// audio buffer to store a sound file

int record_direction = 1;		// Global variables for the audio stream
unsigned int pos = 0;
double angVel = 0;			// angular velocity of the record
double angPos = 0;			// angular position of the record
bool simulationRunning = false;		// a flag that indicates if the haptic simulation is currently running
bool simulationFinished = true;		// a flag that indicates if the haptic simulation has terminated
cFrequencyCounter freqCounterGraphics;	// a frequency counter to measure the simulation graphic rate
cFrequencyCounter freqCounterHaptics;	// a frequency counter to measure the simulation haptic rate
cThread* hapticsThread;			// haptic thread
GLFWwindow* window = NULL;		// a handle to window display context
int width  = 0;				// current width of window
int height = 0;				// current height of window
int swapInterval = 1;			// swap interval for the display context (vertical synchronization)



double p[DHD_MAX_DOF];
double r[3][3];
double v[DHD_MAX_DOF];
double f[DHD_MAX_DOF];
double nullPose[DHD_MAX_DOF] = { 0.0, 0.0, 0.0,  // translation
                                 0.0, 0.0, 0.0,  // rotation (joint angles)
                                 0.0 };          // gripper
int mode = 0;

int    res;

// Setup filter constants

double f_c = 100.0; // cutoff, Hz
double alpha = (2*  M_PI * 0.00025 *f_c) / (2*  M_PI * 0.00025 *f_c + 1)  ;
double f_c_2 = 10000000.0; // cutoff, Hz
double alpha_2 = (2*  M_PI * 0.00025 *f_c) / (2*  M_PI * 0.00025 *f_c + 1)  ;

FeedbackCalculator feedback_calculator;



//------------------------------------------------------------------------------
// DECLARED MACROS
//------------------------------------------------------------------------------

// convert to resource path
#define RESOURCE_PATH(p)    (char*)((resourceRoot+string(p)).c_str())


//------------------------------------------------------------------------------
// DECLARED FUNCTIONS
//------------------------------------------------------------------------------

// callback when the window display is resized
void windowSizeCallback(GLFWwindow* a_window, int a_width, int a_height);

// callback when an error GLFW occurs
void errorCallback(int error, const char* a_description);

// callback when a key is pressed
void keyCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods);

// this function renders the scene
void updateGraphics(void);

// this function contains the main haptics simulation loop
void updateHaptics(void);

// this function closes the application
void close(void);

// this function filters the gain, no annoying pops
double filter_gain(double x , double x_msr, double alpha);

//==============================================================================
/*
    DEMO:    turntable.cpp

    This example demonstrates the use of friction, animation, and
    sound effects.  With the haptic device you can spin the record
    back and forth and at different speeds.
*/
//==============================================================================

int main(int argc, char* argv[])
{
    //--------------------------------------------------------------------------
    // INITIALIZATION
    //--------------------------------------------------------------------------

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << "CHAI3D" << endl;
    cout << "Demo: 24-turntable" << endl;
    cout << "Copyright 2003-2016" << endl;
    cout << "-----------------------------------" << endl << endl << endl;
    cout << "Keyboard Options:" << endl << endl;
    cout << "[f] - Enable/Disable full screen mode" << endl;
    cout << "[m] - Enable/Disable vertical mirroring" << endl;
    cout << "[q] - Exit application" << endl;
    cout << endl << endl;

    // parse first arg to try and locate resources
    string resourceRoot = string(argv[0]).substr(0,string(argv[0]).find_last_of("/\\")+1);

    feedback_calculator.setValues(&mode, &(p[0]), &(v[0]), &(f[0]), &r[0][0], &r[1][0], &r[2][0]);


    //--------------------------------------------------------------------------
    // INITIALISE SIGMA
    //--------------------------------------------------------------------------


  // open the first available device
  if (drdOpen () < 0) {
    printf ("error: cannot open device (%s)\n", dhdErrorGetLastStr ());
    dhdSleep (2.0);
    return -1;
  }

  // print out device identifier
  if (!drdIsSupported ()) {
    printf ("unsupported device\n");
    printf ("exiting...\n");
    dhdSleep (2.0);
    drdClose ();
    return -1;
  }
  printf ("%s haptic device detected\n\n", dhdGetSystemName ());

  // perform auto-initialization
  if (!drdIsInitialized () && drdAutoInit () < 0) {
    printf ("error: auto-initialization failed (%s)\n", dhdErrorGetLastStr ());
    dhdSleep (2.0);
    return -1;
  }
  else if (drdStart () < 0) {
    printf ("error: regulation thread failed to start (%s)\n", dhdErrorGetLastStr ());
    dhdSleep (2.0);
    return -1;
  }


    //--------------------------------------------------------------------------
    // OPEN GL - WINDOW DISPLAY
    //--------------------------------------------------------------------------

    // initialize GLFW library
    if (!glfwInit())
    {
        cout << "failed initialization" << endl;
        cSleepMs(1000);
        return 1;
    }


    // set error callback
    glfwSetErrorCallback(errorCallback);

    // compute desired size of window
    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    int w = 0.8 * mode->height;
    int h = 0.5 * mode->height;
    int x = 0.5 * (mode->width - w);
    int y = 0.5 * (mode->height - h);

    // set OpenGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    // set active stereo mode
    if (stereoMode == C_STEREO_ACTIVE)
    {
        glfwWindowHint(GLFW_STEREO, GL_TRUE);
    }
    else
    {
        glfwWindowHint(GLFW_STEREO, GL_FALSE);
    }

    // create display context
    window = glfwCreateWindow(w, h, "CHAI3D", NULL, NULL);
    if (!window)
    {
        cout << "failed to create window" << endl;
        cSleepMs(1000);
        glfwTerminate();
        return 1;
    }

    // ok til here

    // get width and height of window
    glfwGetWindowSize(window, &width, &height);

    // set position of window
    glfwSetWindowPos(window, x, y);

    // set key callback
    glfwSetKeyCallback(window, keyCallback);

    // set resize callback
    glfwSetWindowSizeCallback(window, windowSizeCallback);

    // set current display context
    glfwMakeContextCurrent(window);

    // sets the swap interval for the current display context
    glfwSwapInterval(swapInterval);

    // OK til here

#ifdef GLEW_VERSION
    // initialize GLEW library
    if (glewInit() != GLEW_OK)
    {
        cout << "failed to initialize GLEW library" << endl;
        glfwTerminate();
        return 1;
    }
#endif


    //--------------------------------------------------------------------------
    // WORLD - CAMERA - LIGHTING
    //--------------------------------------------------------------------------

    // create a new world.
    world = new cWorld();

    // set the background color of the environment
    world->m_backgroundColor.setBlack();

    // create a camera and insert it into the virtual world
    camera = new cCamera(world);
    world->addChild(camera);

    // position and orient the camera
    camera->set(cVector3d (1.70, 0.14, 1.10),    // camera position (eye)
                cVector3d (0.20, 0.14,-0.10),    // lookat position (target)
                cVector3d (0.00, 0.00, 1.00));   // direction of the (up) vector

    // set the near and far clipping planes of the camera
    // anything in front or behind these clipping planes will not be rendered
    camera->setClippingPlanes(0.01, 10.0);

    // set stereo mode
    camera->setStereoMode(stereoMode);

    // set stereo eye separation and focal length (applies only if stereo is enabled)
    camera->setStereoEyeSeparation(0.03);
    camera->setStereoFocalLength(3.0);

    // set vertical mirrored display mode
    camera->setMirrorVertical(mirroredDisplay);

    // create a light source
    light = new cSpotLight(world);

    // attach light to camera
    world->addChild(light);

    // enable light source
    light->setEnabled(true);

    // position the light source
    light->setLocalPos(1.5, 0.40, 1.5);

    // define the direction of the light beam
    light->setDir(-2.0,-0.5,-2.0);

    // enable this light source to generate shadows
    light->setShadowMapEnabled(true);

    // set the resolution of the shadow map
    //light->m_shadowMap->setQualityLow();
    light->m_shadowMap->setQualityMedium();

    // set light cone half angle
    light->setCutOffAngleDeg(30);


    // ok til here

/*
  // move to center
  drdMoveTo (nullPose);

  // request a null force (only gravity compensation will be applied)
  // this will only apply to unregulated axis
  drdSetForceAndTorqueAndGripperForce (0.0, 0.0, 0.0,  // force
                                       0.0, 0.0, 0.0,  // torque
                                       0.0);           // gripper force

*/
    //--------------------------------------------------------------------------
    // HAPTIC DEVICES / TOOLS
    //--------------------------------------------------------------------------

    // create a haptic device handler
    handler = new cHapticDeviceHandler();

    // get access to the first available haptic device found
    handler->getDevice(hapticDevice, 0);

    // retrieve information about the current haptic device
    cHapticDeviceInfo hapticDeviceInfo = hapticDevice->getSpecifications();

    // create a tool (cursor) and insert into the world
    tool = new cToolCursor(world);
    world->addChild(tool);

    // connect the haptic device to the virtual tool
    tool->setHapticDevice(hapticDevice);

    // define the radius of the tool (sphere)
    double toolRadius = 0.01;

    // define a radius for the tool
    tool->setRadius(toolRadius);

    // hide the device sphere. only show proxy.
    tool->setShowContactPoints(true, false);

    // create a white cursor
    tool->m_hapticPoint->m_sphereProxy->m_material->setWhite();

    // enable if objects in the scene are going to rotate of translate
    // or possibly collide against the tool. If the environment
    // is entirely static, you can set this parameter to "false"
    tool->enableDynamicObjects(true);

    // map the physical workspace of the haptic device to a larger virtual workspace.
    tool->setWorkspaceRadius(0.8);

    // haptic forces are enabled only if small forces are first sent to the device;
    // this mode avoids the force spike that occurs when the application starts when 
    // the tool is located inside an object for instance. 
    tool->setWaitForSmallForce(true);


    // start the haptic tool
    tool->start();



    //--------------------------------------------------------------------------
    // CREATE OBJECTS
    //--------------------------------------------------------------------------

    // read the scale factor between the physical workspace of the haptic
    // device and the virtual workspace defined for the tool
    double workspaceScaleFactor = tool->getWorkspaceScaleFactor();

    // stiffness properties
    double maxStiffness	= hapticDeviceInfo.m_maxLinearStiffness / workspaceScaleFactor;

    bool fileload[16];				// loading a file

    // create an audio device to play sounds
    audioDevice = new cAudioDevice();

    // create an audio buffer and load audio wave file
    audioBuffer1 = new cAudioBuffer();
    fileload[0] = audioBuffer1->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sine.wav"));
    if (!fileload[0])
    {
        #if defined(_MSVC)
        fileload[0] = audioBuffer1->loadFromFile("../../../bin/resources/sounds/A_sine.wav");
        #endif                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
    }


    audioBuffer2 = new cAudioBuffer();
    audioBuffer3 = new cAudioBuffer();
    audioBuffer4 = new cAudioBuffer();
    audioBuffer5 = new cAudioBuffer();
    audioBuffer6 = new cAudioBuffer();
    audioBuffer7 = new cAudioBuffer();
    audioBuffer8 = new cAudioBuffer();
    audioBuffer9 = new cAudioBuffer();
    audioBuffer10 = new cAudioBuffer();
    audioBuffer11 = new cAudioBuffer();
    audioBuffer12 = new cAudioBuffer();
    audioBuffer13 = new cAudioBuffer();
    audioBuffer14 = new cAudioBuffer();
    audioBuffer15 = new cAudioBuffer();
    audioBuffer16 = new cAudioBuffer();
    fileload[1] = audioBuffer2->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sine.wav"));
    fileload[2] = audioBuffer3->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sine.wav"));
    fileload[3] = audioBuffer4->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sine.wav"));
    fileload[4] = audioBuffer5->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sine.wav"));
    fileload[5] = audioBuffer6->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sine.wav"));
    fileload[6] = audioBuffer7->loadFromFile(RESOURCE_PATH("../resources/sounds/A_saw.wav"));
    fileload[7] = audioBuffer8->loadFromFile(RESOURCE_PATH("../resources/sounds/A_saw.wav"));
    fileload[8] = audioBuffer9->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sq.wav"));
    fileload[9] = audioBuffer10->loadFromFile(RESOURCE_PATH("../resources/sounds/A_sq.wav"));
    fileload[10] = audioBuffer11->loadFromFile(RESOURCE_PATH("../resources/sounds/n_pink.wav"));
    fileload[11] = audioBuffer12->loadFromFile(RESOURCE_PATH("../resources/sounds/n_pink.wav"));
    fileload[12] = audioBuffer13->loadFromFile(RESOURCE_PATH("../resources/sounds/n_brown.wav"));
    fileload[13] = audioBuffer14->loadFromFile(RESOURCE_PATH("../resources/sounds/n_brown.wav"));
    fileload[14] = audioBuffer15->loadFromFile(RESOURCE_PATH("../resources/sounds/n_white.wav"));
    fileload[15] = audioBuffer16->loadFromFile(RESOURCE_PATH("../resources/sounds/n_white.wav"));

    // check for errors
    if ((!fileload[0]) || (!fileload[1]) || (!fileload[2]) || (!fileload[3]) || (!fileload[4]) || (!fileload[5]) || (!fileload[6]) || (!fileload[7]) || (!fileload[8]) || (!fileload[9]) || (!fileload[10]) || (!fileload[11]) || (!fileload[12]) || (!fileload[13]) || (!fileload[14]) || (!fileload[15]))
    {
        cout << "Error - Sound file failed to load or initialize correctly." << endl;
        close();
        return (-1);
    }



    for (int i=0; i<16; i++)
    {
        audioSource[i] = new cAudioSource();
    }


    audioSource[0]->setAudioBuffer(audioBuffer1);    
    audioSource[1]->setAudioBuffer(audioBuffer2);    
    audioSource[2]->setAudioBuffer(audioBuffer3);    
    audioSource[3]->setAudioBuffer(audioBuffer4);    
    audioSource[4]->setAudioBuffer(audioBuffer5);    
    audioSource[5]->setAudioBuffer(audioBuffer6);    
    audioSource[6]->setAudioBuffer(audioBuffer7);    
    audioSource[7]->setAudioBuffer(audioBuffer8);    
    audioSource[8]->setAudioBuffer(audioBuffer9);    
    audioSource[9]->setAudioBuffer(audioBuffer10);    
    audioSource[10]->setAudioBuffer(audioBuffer11);    
    audioSource[11]->setAudioBuffer(audioBuffer12);    
    audioSource[12]->setAudioBuffer(audioBuffer13);    
    audioSource[13]->setAudioBuffer(audioBuffer14);    
    audioSource[14]->setAudioBuffer(audioBuffer15);    
    audioSource[15]->setAudioBuffer(audioBuffer16);    


    for (int i=0; i<16; i++)
    {
        audioSource[i]->setGain(0.0);
        audioSource[i]->setPitch(1.0);
        audioSource[i]->setLoop(true);
        audioSource[i]->play();
    }

    //--------------------------------------------------------------------------
    // WIDGETS
    //--------------------------------------------------------------------------

    // create a font
    font = NEW_CFONTCALIBRI20();
    
    // create a label to display the haptic and graphic rate of the simulation
    labelRates = new cLabel(font);
    labelRates->m_fontColor.setBlack();
    camera->m_frontLayer->addChild(labelRates);

    // create a background
    background = new cBackground();
    camera->m_backLayer->addChild(background);

    // set background properties
    background->setCornerColors(cColorf(1.0f, 1.0f, 1.0f),
                                cColorf(1.0f, 1.0f, 1.0f),
                                cColorf(0.8f, 0.8f, 0.8f),
                                cColorf(0.8f, 0.8f, 0.8f));




    //--------------------------------------------------------------------------
    // CENTER SIGMA
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    // START SIMULATION
    //--------------------------------------------------------------------------

    // create a thread which starts the main haptics rendering loop
    hapticsThread = new cThread();
    hapticsThread->start(updateHaptics, CTHREAD_PRIORITY_HAPTICS);

    // setup callback when application exits
    atexit(close);


    //--------------------------------------------------------------------------
    // MAIN GRAPHIC LOOP
    //--------------------------------------------------------------------------

    // call window size callback at initialization
    windowSizeCallback(window, width, height);

    // main graphic loop
    while (!glfwWindowShouldClose(window))
    {
        // get width and height of window
        glfwGetWindowSize(window, &width, &height);

        // render graphics
        updateGraphics();

        // swap buffers
        glfwSwapBuffers(window);

        // process events
        glfwPollEvents();

        // signal frequency counter
        freqCounterGraphics.signal(1);
    }

    // close window
    glfwDestroyWindow(window);

    // terminate GLFW library
    glfwTerminate();

    // exit
    return 0;
}

//------------------------------------------------------------------------------

void windowSizeCallback(GLFWwindow* a_window, int a_width, int a_height)
{
    // update window size
    width  = a_width;
    height = a_height;
}

//------------------------------------------------------------------------------

void errorCallback(int a_error, const char* a_description)
{
    cout << "Error: " << a_description << endl;
}
//------------------------------------------------------------------------------

void keyCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods)
{
    // filter calls that only include a key press
    if ((a_action != GLFW_PRESS) && (a_action != GLFW_REPEAT))
    {
        return;
    }

    // option - exit
    else if ((a_key == GLFW_KEY_ESCAPE) || (a_key == GLFW_KEY_Q))
    {
        glfwSetWindowShouldClose(a_window, GLFW_TRUE);
    }

    // option - 0
    else if (a_key == GLFW_KEY_0)
    {
        tool->setForcesOFF();
        mode = 0;
        feedback_calculator.switch_mode();
        tool->setWaitForSmallForce(true);
        tool->setForcesON();
    }

    // option - 1
    else if (a_key == GLFW_KEY_1)
    {
        tool->setForcesOFF();
        mode = 1;
        feedback_calculator.switch_mode();
        tool->setWaitForSmallForce(true);
        tool->setForcesON();
    }

    // option - 2
    else if (a_key == GLFW_KEY_2)
    {
        tool->setForcesOFF();
        mode = 2;
        feedback_calculator.switch_mode();
        tool->setWaitForSmallForce(true);
        tool->setForcesON();
    }


    // option - 3
    else if (a_key == GLFW_KEY_3)
    {
        tool->setForcesOFF();
        mode = 3;
        feedback_calculator.switch_mode();
        tool->setWaitForSmallForce(true);
        tool->setForcesON();
    }



    // option - 4
    else if (a_key == GLFW_KEY_4)
    {
        tool->setForcesOFF();
        mode = 4;
        feedback_calculator.switch_mode();
        tool->setWaitForSmallForce(true);
        tool->setForcesON();
    }






    // option - toggle fullscreen
    else if (a_key == GLFW_KEY_F)
    {
        // toggle state variable
        fullscreen = !fullscreen;

        // get handle to monitor
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();

        // get information about monitor
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);

        // set fullscreen or window mode
        if (fullscreen)
        {
            glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
            glfwSwapInterval(swapInterval);
        }
        else
        {
            int w = 0.8 * mode->height;
            int h = 0.5 * mode->height;
            int x = 0.5 * (mode->width - w);
            int y = 0.5 * (mode->height - h);
            glfwSetWindowMonitor(window, NULL, x, y, w, h, mode->refreshRate);
            glfwSwapInterval(swapInterval);
        }
    }

    // option - toggle vertical mirroring
    else if (a_key == GLFW_KEY_M)
    {
        mirroredDisplay = !mirroredDisplay;
        camera->setMirrorVertical(mirroredDisplay);
    }
}

//------------------------------------------------------------------------------

void close(void)
{
    // stop the simulation
    simulationRunning = false;

    // wait for graphics and haptics loops to terminate
    while (!simulationFinished) { cSleepMs(100); }

    // close haptic device
    tool->stop();

    // delete resources
    delete hapticsThread;
    delete world;
    delete handler;
    delete audioDevice;
    delete audioBuffer1;
    for (int i=0; i<16; i++)
    {
        delete audioSource[i];
    }
    delete audioBuffer2;
    delete audioBuffer3;
    delete audioBuffer4;
    delete audioBuffer5;
    delete audioBuffer6;
    delete audioBuffer7;
    delete audioBuffer8;
    delete audioBuffer9;
    delete audioBuffer10;
    delete audioBuffer11;
    delete audioBuffer12;
    delete audioBuffer13;
    delete audioBuffer14;
    delete audioBuffer15;
    delete audioBuffer16;

}

//------------------------------------------------------------------------------

void updateGraphics(void)
{
    /////////////////////////////////////////////////////////////////////
    // UPDATE WIDGETS
    /////////////////////////////////////////////////////////////////////

    // update haptic and graphic rate data
    labelRates->setText(cStr(freqCounterGraphics.getFrequency(), 0) + " Hz / " +
        cStr(freqCounterHaptics.getFrequency(), 0) + " Hz");

    // update position of label
    labelRates->setLocalPos((int)(0.5 * (width - labelRates->getWidth())), 15);


    /////////////////////////////////////////////////////////////////////
    // RENDER SCENE
    /////////////////////////////////////////////////////////////////////

    // update shadow maps (if any)
    world->updateShadowMaps(false, mirroredDisplay);

    // render world
    camera->renderView(width, height);

    // wait until all GL commands are completed
    glFinish();

    // check for any OpenGL errors
    GLenum err = glGetError();
    if (err != GL_NO_ERROR) cout << "Error: " << gluErrorString(err) << endl;
}

//------------------------------------------------------------------------------

void updateHaptics(void)
{
    // reset clock
    cPrecisionClock clock;
    clock.reset();

    // simulation in now running
    simulationRunning  = true;
    simulationFinished = false;
    double pitch = 0;
    double pitch_msr = 0;
    double gain_msr = 0;
    double gain[16];
    double gain_unf[16];

    // main haptic simulation loop
    while(simulationRunning)
    {
        /////////////////////////////////////////////////////////////////////
        // SIMULATION TIME
        /////////////////////////////////////////////////////////////////////

        // stop the simulation clock
        clock.stop();

        // read the time increment in seconds
        double timeInterval = clock.getCurrentTimeSeconds();

        // restart the simulation clock
        clock.reset();
        clock.start();

        // signal frequency counter
        freqCounterHaptics.signal(1);


        /////////////////////////////////////////////////////////////////////
        // HAPTIC FORCE COMPUTATION
        /////////////////////////////////////////////////////////////////////

        // compute global reference frames for each object
        world->computeGlobalPositions(true);

        // update position and orientation of tool
        tool->updateFromDevice();

        // compute interaction forces
     //   tool->computeInteractionForces();

        // send forces to haptic device
    //    tool->applyToDevice();


        /////////////////////////////////////////////////////////////////////
        // ANIMATION
        /////////////////////////////////////////////////////////////////////
        drdGetPositionAndOrientation (&(p[0]), &(p[1]), &(p[2]),
                                      &(p[3]), &(p[4]), &(p[5]),
                                      &(p[6]), r);


        drdGetVelocity (&(v[0]), &(v[1]), &(v[2]),
                        &(v[3]), &(v[4]), &(v[5]),
                        &(v[6]));



        feedback_calculator.calculate_feedback();


        // apply centering force with damping
    //    res = drdSetForceAndTorqueAndGripperForce (f[0], f[1], f[2],  // force
    //                                           f[3], f[4], f[5],  // torque
    //                                           f[6]);             // gripper force
    //    if (res < DHD_NO_ERROR) {
    //      printf ("error: cannot set force (%s)\n", dhdErrorGetLastStr ());
     //     simulationRunning = false;
    //    }



    //    gain = std::min(std::max( (0.025 - p[6]) / 0.025 , 0.0 ) , 1.0 );


        /////////////////////////////////////////////////////////////////////
        // CALCULATE INPUT
        /////////////////////////////////////////////////////////////////////

       
        switch (mode) {
          case 0 :
            gain_msr = std::min(std::max( (p[5] ) / 1.0 , 0.0 ) , 1.0 );
            pitch = 0.5 + 1.5 * (p[2] - MIN_Z) / (MAX_Z - MIN_Z);
            break;

          case 1 :
            gain_msr = std::min(std::max( 50 * (p[1] ) / 1.0 , 0.0 ) , 1.0 );
            pitch = pow(2.0, p[2]/0.120) * pow(1.2, p[4]);
            break;

          case 2 :
            gain_msr = std::min(std::max( 50 * (p[1] ) / 1.0 , 0.0 ) , 1.0 );
            pitch = pow(2.0, p[2]/0.120) * pow(0.86, p[3]);
            break;

          case 3 :
            gain_msr = std::min(std::max( -100 * (p[6] - 0.015) / 1.0 , 0.0 ) , 1.0 );
            pitch = pow(2.0, p[2]/0.120) * pow(0.86, p[3]);
            break;

          case 4 :
            gain_msr = std::min(std::max( -50 * (p[2] ) / 1.0 , 0.0 ) , 1.0 );
            pitch_msr = pow(2.0, p[1]/0.120) * pow(0.86, p[3]);
            break;


        }



        pitch = filter_gain(pitch, pitch_msr, alpha);



        audioSource[0]->setPitch(pitch);
        if (p[5]>0.0) {
          audioSource[1]->setPitch(pitch*1.5);
          audioSource[2]->setPitch(pitch*2);
          audioSource[3]->setPitch(pitch*2.5);
          audioSource[4]->setPitch(pitch*3);
          audioSource[5]->setPitch(pitch*3.5);
        }
        else {
          audioSource[1]->setPitch(pitch*0.5);
          audioSource[2]->setPitch(pitch*0.75);
          audioSource[3]->setPitch(pitch*1.25);
          audioSource[4]->setPitch(pitch*1.5);
          audioSource[5]->setPitch(pitch*1.75);
        }
        gain_unf[1]=(std::min(std::max(  gain_msr*0.5*abs(p[5])-0.05 , 0.0 ) , 1.0 ));
        gain_unf[2]=(std::min(std::max( gain_msr*0.25*abs(p[5])-0.05 , 0.0 ) , 1.0 ));
        gain_unf[3]=(std::min(std::max( gain_msr*0.125*abs(p[5])-0.05 , 0.0 ) , 1.0 ));
        gain_unf[4]=(std::min(std::max( gain_msr*0.0625*abs(p[5])-0.05 , 0.0 ) , 1.0 ));
        gain_unf[5]=(std::min(std::max( gain_msr*0.03125*abs(p[5])-0.05 , 0.0 ) , 1.0 ));
/*
        audioSource[13]->setGain(gain*0.25*abs(p[0]));
        audioSource[11]->setGain(gain*0.5*abs(p[6]));
        audioSource[12]->setGain(gain*0.5*abs(p[0]));
        audioSource[10]->setGain(gain*0.5*abs(p[6]));
        audioSource[12]->setPitch(abs(p[5]));
        audioSource[10]->setPitch(abs(p[5]));
*/
        if (p[0]<0.0) {
        gain_unf[6]=(gain_msr*abs(p[0])*0.5);
        audioSource[6]->setPitch(pitch*0.125);
        gain_unf[7]=(gain_msr*abs(p[0])*0.5);
        audioSource[7]->setPitch(pitch*0.25);
        }
        else {
        gain_unf[8]=(gain_msr*abs(p[0])*0.5);
        audioSource[8]->setPitch(pitch*0.125);
        gain_unf[9]=(gain_msr*abs(p[0])*0.5);
        audioSource[9]->setPitch(pitch*0.25);
        }

        if (p[6]>0.0) {
        gain_unf[14]=(gain_msr*abs(p[6])*1.6);
        audioSource[14]->setPitch(pitch*0.5);
        gain_unf[15]=(gain_msr*abs(p[6])*3.2);
        audioSource[15]->setPitch(pitch*1.0);
        }
        else {
        gain_unf[14]=(gain_msr*abs(p[6])*1.6);
        audioSource[15]->setPitch(pitch*2.0);
        gain_unf[14]=(gain_msr*abs(p[6])*3.2);
        audioSource[15]->setPitch(pitch*1.0);
        }

        gain_unf[0]=(std::min(std::max( gain_msr - abs(p[5]) , 0.0 ) , 1.0 ));
        //printf("gain_msr: %3.3f, gain_unf: %3.3f, gain: %3.3f\n", gain_msr, gain_unf[0], gain[0]);
        for (int i = 0; i < 16; i++)  {
            gain[i] = filter_gain(gain[i], gain_unf[i], alpha);
            audioSource[i]->setGain(gain[i]);

        }
        
  //      printf ("pitch %+0.03f gain %+0.03f gain msr %+0.03f alpha %+0.06f \n", pitch, gain, p[5], alpha);



        cVector3d torque(0,0,0);
        cVector3d force(0,0,0);
        force.x(f[0]);
        force.y(f[1]);
        force.z(f[2]);
        torque.x(f[3]);
        torque.y(f[4]);
        torque.z(f[5]);


        // compute interaction forces
   //     tool->computeInteractionForces();
        printf ("F %+0.03f , %+0.03f , %+0.03f , %+0.03f , %+0.03f , %+0.03f , %+0.03f \r", force.x(), force.y(), force.z(), torque.x(), torque.y(), torque.z(), f[6]);

        tool->setDeviceGlobalForce(force);
        tool->setDeviceGlobalTorque(torque);
        tool->setGripperForce(f[6]);

        // send forces to haptic device
        tool->applyToDevice();

    }

    // exit haptics thread
    simulationFinished = true;
}


double filter_gain(double x , double x_msr, double alpha) {

  x =  (alpha * x_msr + (1-alpha) * x);
  return x;
}


//------------------------------------------------------------------------------
