

#include "dhdc.h"
#include "drdc.h"

#ifndef FEEDBACK_H
#define FEEDBACK_H
#include "Eigen/Eigen"
using namespace Eigen;


class FeedbackCalculator
{
  public:
  int * mode = 0;
  double * p0; // start of position array
  double * v0; // start of velocity array
  double * f0; // start of force array
  double * row0; // start of rotation row 0
  double * row1; // start of rotation row 1
  double * row2; // start of rotation row 2
//  double * distance; // distance of rover for feedback

  double MAXF =  10.0;
  double KP_m[3] = {100.0, 100.0, 100.0};
  double KVP_m[3] = {10.0, 10.0, 10.0};

  double MAXT  =  1.0;
  double KR_m[3] = {0.9, 0.9, 0.9};
  double KWR_m[3] = {0.06, 0.06, 0.06};

  double KG =   100.0;
  double KVG   =  5.0;
  double MAXG   = 2.0;
  Matrix3d rotation;
  Matrix3d center;

  double normf;
  double normt;
  double driving_or_camera = 0.5;

  FeedbackCalculator();
  void setValues(int * mode_, double *p0_, double *v0_, double *f0_, double * row0_, double * row1_, double * row2_);
  void switch_mode();
  void calculate_feedback();
//  void calculate_forces();
//  void calculate_feedback_toy_car();
//  void calculate_feedback_steering_wheel();
//  void calculate_feedback_third_mode();
//  void recalculate_wall_stiffness();
};

#endif
