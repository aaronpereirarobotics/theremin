
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include "Eigen/Eigen"
using namespace Eigen;

#include "dhdc.h"
#include "drdc.h"

#include "feedback.h"

FeedbackCalculator::FeedbackCalculator()  {
}

void FeedbackCalculator::setValues(int * mode_, double *p0_, double *v0_, double *f0_, double * row0_, double * row1_, double * row2_)  {
  mode = mode_;
  p0 = p0_;  
  v0 = v0_;  
  f0 = f0_;  
  row0 = row0_;
  row1 = row1_;
  row2 = row2_;

  center.setIdentity ();                           // rotation (matrix)
}


void FeedbackCalculator::switch_mode() {
  switch (*mode) {
    case 0 :
      //reset the gains
       KP_m[0] = 100.0;
       KP_m[1] = 100.0;
       KP_m[2] = 0.0;
       KVP_m[0] = 10.0;
       KVP_m[1] = 10.0;
       KVP_m[2] = 1.0;

       KR_m[0] = 0.9;
       KR_m[1] = 0.6;
       KR_m[2] = 0.6;
       KWR_m[0] = 0.06;
       KWR_m[1] = 0.04;
       KWR_m[2] = 0.04;

       drdRegulatePos  (0);

      break;
    case 1 :
      //reset the gains
       KP_m[0] = 100.0;
       KP_m[1] = 600.0;
       KP_m[2] = 0.0;
       KVP_m[0] = 10.0;
       KVP_m[1] = 20.0;
       KVP_m[2] = 1.0;

       KR_m[0] = 0.6;
       KR_m[1] = 0.3;
       KR_m[2] = 0.6;
       KWR_m[0] = 0.04;
       KWR_m[1] = 0.02;
       KWR_m[2] = 0.04;


       drdRegulatePos  (0);

      break;
    case 2 :
      //reset the gains
       KP_m[0] = 100.0;
       KP_m[1] = 600.0;
       KP_m[2] = 0.0;
       KVP_m[0] = 10.0;
       KVP_m[1] = 20.0;
       KVP_m[2] = 5.0;

       KR_m[0] = 0.3;
       KR_m[1] = 0.6;
       KR_m[2] = 0.6;
       KWR_m[0] = 0.02;
       KWR_m[1] = 0.04;
       KWR_m[2] = 0.04;

       drdRegulatePos  (0);

      break;
    case 3 :
      //reset the gains
       KP_m[0] = 100.0;
       KP_m[1] = 100.0;
       KP_m[2] = 0.0;
       KVP_m[0] = 10.0;
       KVP_m[1] = 10.0;
       KVP_m[2] = 1.0;

       KR_m[0] = 0.6;
       KR_m[1] = 0.6;
       KR_m[2] = 0.6;
       KWR_m[0] = 0.04;
       KWR_m[1] = 0.04;
       KWR_m[2] = 0.04;

       drdRegulatePos  (0);

      break;
    case 4 :
      //reset the gains
       KP_m[0] = 100.0;
       KP_m[1] = 0.0;
       KP_m[2] = 600.0;
       KVP_m[0] = 10.0;
       KVP_m[1] = 1.0;
       KVP_m[2] = 5.0;

       KR_m[0] = 0.3;
       KR_m[1] = 0.1;
       KR_m[2] = 0.6;
       KWR_m[0] = 0.02;
       KWR_m[1] = 0.005;
       KWR_m[2] = 0.04;

       drdRegulatePos  (0);

      break;
  }
}




void FeedbackCalculator::calculate_feedback() {



    rotation.row(0) = Vector3d::Map(row0, 3);
    rotation.row(1) = Vector3d::Map(row1, 3);
    rotation.row(2) = Vector3d::Map(row2, 3);

    Map<Vector3d> force   (f0, 3);
    Map<Vector3d> torque  (f0+3, 3);

    // compute base centering force
    *(f0)   = - KP_m[0] * *(p0);
    *(f0+1) = - KP_m[1] * *(p0+1);
    *(f0+2) = - KP_m[2] * *(p0+2);

    // compute wrist centering torque
    AngleAxisd deltaRotation (rotation.transpose() * center);
    torque = rotation * (deltaRotation.angle() * deltaRotation.axis());
    *(f0+3) = KR_m[0] * *(f0+3);
    *(f0+4) = KR_m[1] * *(f0+4);
    *(f0+5) = KR_m[2] * *(f0+5);

    // compute gripper centering force
    if ((*mode == 3)||(*mode == 4))  {
      *(f0+6)  = - KG * (*(p0+6) - 0.025);
    } else {
      *(f0+6)  = 0.0;
    }

    // scale force to a pre-defined ceiling
    if ((normf = force.norm()) > MAXF) force *= MAXF/normf;

    // scale torque to a pre-defined ceiling
    if ((normt = torque.norm()) > MAXT) torque *= MAXT/normt;

    // scale gripper force to a pre-defined ceiling
    if (*(f0+6) >  MAXG) *(f0+6) =  MAXG;
    if (*(f0+6) < -MAXG) *(f0+6) = -MAXG;

    // add correcting force
    if ((*mode == 2) && (*(p0+1) > 0))  {
      *(f0+2) += ( fmod( (*(p0+2) + 0.150 + 0.005) , 0.01 ) - 0.005 ) * KP_m[1] * *(p0+1) * -100.0;
    }


    // add correction in damping
    if (*mode == 4) {
      KVP_m[2] = std::min( std::max( 20.0 - abs(*(p0+4)) * 20.0 , 3.0 ) , 20.0 );
      KWR_m[0] = std::min( std::max( 0.02 + abs(*(p0+4)) * 0.1 , 0.02 ) , 0.2 );
      KR_m[0] = std::min( std::max( 0.3 + abs(*(p0+4)) * 1.5 , 0.3 ) , 3.0 );
    }

    // add damping
    *(f0)    -= KVP_m[0] * *(v0);
    *(f0+1)  -= KVP_m[1] * *(v0+1);
    *(f0+2)  -= KVP_m[2] * *(v0+2);
    *(f0+3)  -= KWR_m[0] * *(v0+3);
    *(f0+4)  -= KWR_m[1] * *(v0+4);
    *(f0+5)  -= KWR_m[2] * *(v0+5);
    *(f0+6)  -= KVG * *(v0+6);

    if (((*mode == 1) || (*mode == 2)) && (*(p0+1) < 0))  {
      *(f0+1) = 0.0;
    }


    if ((*mode == 4) && (*(p0+2) > 0))  {
      *(f0+2) = 0.0;
    }

}


