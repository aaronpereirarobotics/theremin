
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include "Eigen/Eigen"
//using namespace Eigen;
using namespace std;

#include "dhdc.h"
#include "drdc.h"

#include "input.h"


/*
  private:
  int must_brake_counter = 30;
  int executing_debrake_sequence = 100;

  public:
  int mode = 0;
  double *p[DHD_MAX_DOF];
  double *throttle;
  double *steer;
  double *pan;
  double *tilt;

*/

InputCalculator::InputCalculator()  {
 // mode = 0;
//  int brake_counter = 40;
 // int t1 = 25;
 // int t2 = 20;
 // double epsilon = 1.0;
}

void InputCalculator::setValues(int * mode_, double * p0_, double * throttle_, double * steer_, double * pan_, double * tilt_, double * turn_)  {
  mode = mode_;
  p0 = p0_;  /*
  p[0] = p_[0];
  p[1] = p_[1];
  p[2] = p_[2];
  p[3] = p_[3];
  p[4] = p_[4];
  p[5] = p_[5];
  p[6] = p_[6];*/
//  printf("p = %0.2f , %0.2f , %0.2f , %0.2f , %0.2f , %0.2f , %0.2f\r" , *(p0),*(p0+1),*(p0+2),*(p0+3),*(p0+4),*(p0+5),*(p0+6));
  throttle = throttle_;
  steer = steer_;
  pan = pan_;
  tilt = tilt_;
  turn = turn_;

  brake_counter = 40;
  t1 = 25;
  t2 = 20;
  epsilon = 1.0;
}

void InputCalculator::calculate_input() {
 // printf("p = %0.2f , %0.2f , %0.2f , %0.2f , %0.2f , %0.2f , %0.2f",*p[0],*p[1],*p[2],*p[3],*p[4],*p[5],*p[6]);
  switch (*mode) {
    case 0 :
      calculate_input_toy_car();
      break;
    case 1 :
      calculate_input_steering_wheel();
      break;
    case 2 :
      calculate_input_joystick_mode();
      break;
    case 3 :
      calculate_input_spot_turn();
      break;
  }
}

void InputCalculator::calculate_input_toy_car() {

  double Kp_throttle = -2000.0/180.0; // here it will not max out before the mechanical limits in x
  double Kp_steer = -100.0/180.0*3.14;
  double Kd_pan = 15.0/40.0;
  double Kd_tilt = 15.0/40.0;
  double max_throttle = 1.0;
  double min_throttle = -1.0;
  double max_steer = 0.5;
  double min_steer = -0.5;
  double goingForwardsOrBackwards;
  double beta = 1.0;
  double entire_brake_time = 40;

  *turn = 0.0;
  if (*(p0 + 6) < 0.005) {
    // if the gripper is closed, you are driving

    // set throttle
    *throttle = min(max( Kp_throttle * *(p0), min_throttle) , max_throttle);

    // regulate throttle backwards while braking
    if (*throttle > (epsilon))  {
      brake_counter = entire_brake_time;
    } else if (brake_counter > 0) {
      brake_counter--;
      if (brake_counter > t1) {
        beta = ((double)brake_counter - t1)/(entire_brake_time - t1);
      } else if (brake_counter < t2) {
        beta = (((double) t2 - brake_counter) / t2);
      } else {
        beta = 0;
      }

      *throttle = (*throttle ) * beta + epsilon;
    }


    // set steering
    goingForwardsOrBackwards = min(max((*throttle)/10.0,-1.0),1.0);
    *steer = min(max( Kp_steer * *(p0 + 5)*goingForwardsOrBackwards , min_steer) , max_steer);
  } 
  else {
    if (*(p0 + 6) > 0.02) {
      // else you are controlling the pan-tilt
   // THIS PERFORMED POORLY ON THE FIRST TEST ON 2.5.20
   //   *pan = *pan + Kd_pan * *(p0 + 1);
   //   *tilt = *tilt + Kd_tilt * *(p0 + 2);
      *pan = min(max( Kd_pan * *(p0 + 5), -1.0) , 1.0 );
      *tilt = min(max( Kd_tilt * *(p0 + 4), -1.0) , 1.0 );
 
    }
    *throttle = 0;
    *steer = 0;

  }

}

void InputCalculator::calculate_input_steering_wheel() {


  double Kp_throttle = -2000.0/180.0; // here it will not max out before the mechanical limits in x
  double Kp_steer = -100.0/180.0*3.14;
  double Kd_pan = 15.0/40.0;
  double Kd_tilt = 15.0/40.0;
  double max_throttle = 1.0;
  double min_throttle = -1.0;
  double max_steer = 0.5;
  double min_steer = -0.5;
  double goingForwardsOrBackwards;
  double beta = 1.0;
  double entire_brake_time = 40;

  *turn = 0.0;
  if (*(p0 + 6) < 0.005) {
    // if the gripper is closed, you are driving

    // set throttle
    *throttle = min(max( Kp_throttle * *(p0+2), min_throttle) , max_throttle);


    // regulate throttle backwards while braking
    if (*throttle > (epsilon))  {
      brake_counter = entire_brake_time;
    } else if (brake_counter > 0) {
      brake_counter--;
      if (brake_counter > t1) {
        beta = ((double)brake_counter - t1)/(entire_brake_time - t1);
      } else if (brake_counter < t2) {
        beta = (((double) t2 - brake_counter) / t2);
      } else {
        beta = 0;
      }

      *throttle = (*throttle) * beta + epsilon;
    }



    // set steering
 //   goingForwardsOrBackwards = min(max((*throttle-90.0)/10.0,-1.0),1.0);
    *steer = min(max( Kp_steer * *(p0 + 3) , min_steer) , max_steer);
  } 
  else {
    if (*(p0 + 6) > 0.02) {
      // else you are controlling the pan-tilt
   // THIS PERFORMED POORLY ON THE FIRST TEST ON 2.5.20
   //   *pan = *pan + Kd_pan * *(p0 + 1);
   //   *tilt = *tilt + Kd_tilt * *(p0 + 2);
      *pan = min(max( Kd_pan * *(p0 + 5), -1.0) , 1.0 );
      *tilt = min(max( Kd_tilt * *(p0 + 4), -1.0) , 1.0 );
 
    }
    *throttle = 0;
    *steer = 0;
  }

}

void InputCalculator::calculate_input_joystick_mode() {

  double Kp_throttle = -100.0/180.0; // here it will not max out before the mechanical limits in x
  double Kp_steer = -100.0/180.0*3.14;
  double Kd_pan = 15.0/40.0;
  double Kd_tilt = 15.0/40.0;
  double max_throttle = 1.0;
  double min_throttle = -1.0;
  double max_steer = 0.5;
  double min_steer = -0.5;
  double goingForwardsOrBackwards;
  double beta = 1.0;
  double entire_brake_time = 40;

  *turn = 0.0;
  if (*(p0 + 6) < 0.005) {
    // if the gripper is closed, you are driving

    // set throttle
    *throttle = min(max(Kp_throttle * *(p0+4), min_throttle) , max_throttle);


    // regulate throttle backwards while braking
    if (*throttle > (epsilon))  {
      brake_counter = entire_brake_time;
    } else if (brake_counter > 0) {
      brake_counter--;
      if (brake_counter > t1) {
        beta = ((double)brake_counter - t1)/(entire_brake_time - t1);
      } else if (brake_counter < t2) {
        beta = (((double) t2 - brake_counter) / t2);
      } else {
        beta = 0;
      }

      *throttle = (*throttle) * beta + epsilon;
    }




    // set steering
 //   goingForwardsOrBackwards = min(max((*throttle-90.0)/10.0,-1.0),1.0);
    *steer = min(max( Kp_steer * *(p0 + 3) , min_steer) , max_steer);
  } 
  else {
    if (*(p0 + 6) > 0.02) {
      // else you are controlling the pan-tilt
   // THIS PERFORMED POORLY ON THE FIRST TEST ON 2.5.20
   //   *pan = *pan + Kd_pan * *(p0 + 1);
   //   *tilt = *tilt + Kd_tilt * *(p0 + 2);
      *pan = min(max( Kd_pan * *(p0 + 5), -1.0) , 1.0 );
      *tilt = min(max( Kd_tilt * *(p0 + 4), -1.0) , 1.0 );
 
    }
    *throttle = 0;
    *steer = 0;
  }


}



void InputCalculator::calculate_input_spot_turn() {

  double Kp_turn = -100.0/180.0*3.14;
  double Kd_pan = 15.0/40.0;
  double Kd_tilt = 15.0/40.0;
  double max_turn = 0.5;
  double min_turn = -0.5;
  double goingForwardsOrBackwards;
  double beta = 1.0;
  double entire_brake_time = 40;

  *throttle = 0;
  *steer = 0;
  if (*(p0 + 6) < 0.005) {
    // if the gripper is closed, you are driving

    // set turn
    *turn = min(max(Kp_turn * *(p0+5), min_turn) , max_turn);
  }
  else {
    if (*(p0 + 6) > 0.02) {
      // else you are controlling the pan-tilt
   // THIS PERFORMED POORLY ON THE FIRST TEST ON 2.5.20
   //   *pan = *pan + Kd_pan * *(p0 + 1);
   //   *tilt = *tilt + Kd_tilt * *(p0 + 2);
      *pan = min(max( Kd_pan * *(p0 + 5), -1.0) , 1.0 );
      *tilt = min(max( Kd_tilt * *(p0 + 4), -1.0) , 1.0 );
 
    }
    *turn = 0;
  }


}

